package editing;

import javafx.scene.image.Image;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by Greg on 24.05.2017.
 */
public class TextScannerFromXML implements TextScannerInterface {
    double originalImageHeigth;
    double originalImageWidth;
    private String filePath;
    private Document document;
    private Image image;
    //metaData
    private String description;
    private double resolution;
    private String dimension;
    private String imageName;

    public TextScannerFromXML(String filePath) {
        this.filePath = filePath;
        this.document = loadDocument();
        this.description = document.getElementsByTagName("description").item(0).getTextContent();
        this.imageName = document.getElementsByTagName("image-file").item(0).getTextContent();
        this.resolution = (isDouble(document.getElementsByTagName("resolution").item(0).getTextContent())) ?
                Double.parseDouble(document.getElementsByTagName("resolution").item(0).getTextContent()) : 1;
        this.dimension = ((Element) document.getElementsByTagName("resolution").item(0)).getAttribute("unit");
    }
    public Document loadDocument() {
        try {
            File file = new File(filePath);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
            return document;
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public String getDescription() {
        return description;
    }
    @Override
    public double getResolution() {
        return resolution;
    }
    @Override
    public String getDimension() {
        return dimension;
    }
    @Override
    public String getImageName() {
        return imageName;
    }
    @Override
    public Image setImage(String path) {
        this.image = new Image("file:" + path);
        originalImageHeigth = image.getHeight();
        originalImageWidth = image.getWidth();
        return image;
    }

    @Override
    public double getHeight() {
        return originalImageHeigth;
    }

    @Override
    public double getWidth() {
        return originalImageWidth;
    }

    private Boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

