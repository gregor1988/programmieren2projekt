package editing;

import javafx.scene.image.Image;

public interface TextScannerInterface {

    String getDescription();
    double getResolution();
    String getDimension();
    String getImageName();

    Image setImage(String nameOfFile);

    Image getImage();

    double getHeight();

    double getWidth();
}
