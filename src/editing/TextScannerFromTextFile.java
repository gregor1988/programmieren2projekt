package editing;

import javafx.scene.image.Image;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class TextScannerFromTextFile implements TextScannerInterface {
    double originalImageHeigth;
    double originalImageWidth;
    private Image image;
    private String filePath;
    private ArrayList<String> contentOfMetadataFile = new ArrayList<>();
    //metaData
    private String description;
    private double resolution;
    private String dimension;
    private String imageName;
    public TextScannerFromTextFile(String filePath) {
        this.filePath = filePath;
        this.contentOfMetadataFile = loadMetaData();
        setMetadata();


    }
    @Override
    public Image setImage(String path) {
        this.image = new Image("file:" + path);
        this.originalImageHeigth = image.getHeight();
        this.originalImageWidth = image.getWidth();
        return image;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public String getDescription() {
        return description;
    }
    @Override
    public double getResolution() {
        return resolution;
    }
    @Override
    public String getDimension() {
        return dimension;
    }
    @Override
    public String getImageName() {
        return imageName;
    }

    @Override
    public double getHeight() {
        return originalImageHeigth;
    }

    @Override
    public double getWidth() {
        return originalImageWidth;
    }

    private ArrayList<String> loadMetaData() {
        File file = new File(filePath);
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String word = scanner.next();
                contentOfMetadataFile.add(word);
            }
        } catch (IOException e) {
        }

        return contentOfMetadataFile;

    }
    private void setMetadata() {
        for (String word : contentOfMetadataFile
                ) {
            int listIndex;
            if (word.toLowerCase().contains("description:")) {
                StringBuilder descriptionText = new StringBuilder();
                listIndex = contentOfMetadataFile.indexOf(word);
                do {
                    listIndex++;
                    descriptionText.append(contentOfMetadataFile.get(listIndex) + " ");
                } while ((!contentOfMetadataFile.get(listIndex + 1).toLowerCase().contains("resolution:")
                        && (!contentOfMetadataFile.get(listIndex + 1).toLowerCase().contains("image-file:"))) &&
                        (listIndex < contentOfMetadataFile.size() - 2));
                if (listIndex == contentOfMetadataFile.size() - 2) {
                    descriptionText.append(contentOfMetadataFile.get(listIndex + 1));
                }
                description = descriptionText.toString();
            } else if (word.toLowerCase().contains("resolution:")) {
                listIndex = contentOfMetadataFile.indexOf(word);
                try {
                    String nextElement = contentOfMetadataFile.get(listIndex + 1);
                    resolution = (isDouble(nextElement)) ? Double.parseDouble(nextElement) : 1;
                } catch (Exception e) {
                    resolution = 1;
                    dimension = "Pixels";
                }
                if ((!contentOfMetadataFile.get(listIndex + 1).toLowerCase().contains("resolution:")) &&
                        (!contentOfMetadataFile.get(listIndex + 1).toLowerCase().contains("image-file:")) &&
                        (isDouble(contentOfMetadataFile.get(listIndex + 1)))) {
                    try {
                        dimension = contentOfMetadataFile.get(listIndex + 2);
                    } catch (Exception e) {
                        resolution = 1;
                        dimension = "Pixels";
                    }
                } else {
                    dimension = "Pixels";
                }
            } else if (word.toLowerCase().contains("image-file:")) {
                listIndex = contentOfMetadataFile.indexOf(word);
                imageName = contentOfMetadataFile.get(listIndex + 1);
            }
        }
    }
    Boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
