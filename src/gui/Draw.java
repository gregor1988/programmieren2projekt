package gui;

import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ArcType;

public class Draw {
    protected double lengthOfNewLine;
    protected double lenghtOfAllLines;
    protected double extendAngle;
    private double initialX, initialY, endX, endY;
    private double directionXOne, directionYOne, directionXTwo, directionYTwo;
    private double arcSize, startAngle, endAngle;
    private Boolean clickFlag = false;
    private Boolean angleExists = false;
    private Components components;

    public Draw(Components components) {
        this.components = components;

    }

    StackPane drawSingleLineMode(MouseEvent event, StackPane stack, Paint colour, int lineWidth, ImageView imageviewer) {
        if (!clickFlag) {
            initialX = event.getX();
            initialY = event.getY();
            flipClickFlag();
        } else {
            endX = event.getX();
            endY = event.getY();
            flipClickFlag();
            setLengthOfLine();
            setSumOfLineLength();
        }
        if (!clickFlag) {
            return drawLine(stack, colour, lineWidth, imageviewer);
        }
        return stack;
    }

    StackPane drawContinuousLineMode(MouseEvent event, StackPane stack, Paint colour, int lineWidth, ImageView imageviewer) {
        if (!clickFlag) {
            initialX = event.getX();
            initialY = event.getY();
            flipClickFlag();
        } else {
            endX = event.getX();
            endY = event.getY();
            flipClickFlag();
            setLengthOfLine();
            setSumOfLineLength();
        }
        if (!clickFlag) {
            stack = drawLine(stack, colour, lineWidth, imageviewer);
            clickFlag = true;
            initialX = endX;
            initialY = endY;
        }
        return stack;
    }

    StackPane measureAngleMode(MouseEvent event, StackPane stack, Paint color, int lineWidth, ImageView imageViewer) {
        if (!clickFlag) {
            initialX = event.getX();
            initialY = event.getY();
            flipClickFlag();
        } else if (clickFlag && angleExists) {
            endX = event.getX();
            endY = event.getY();
            directionXTwo = endX - initialX;
            directionYTwo = initialY - endY;
            arcSize = getSmalerValue(Math.sqrt((Math.pow(directionXTwo, 2) + Math.pow(directionYTwo, 2))),
                    Math.sqrt(Math.pow(directionXOne, 2) + Math.pow(directionYOne, 2)));
            computeAngles();
            angleExists = false;
            flipClickFlag();
            drawLine(stack, color, lineWidth, imageViewer);
            drawArc(stack, color, lineWidth, imageViewer);
            if (extendAngle < 0) extendAngle = -extendAngle;
            return stack;
        } else {
            endX = event.getX();
            endY = event.getY();
            directionXOne = endX - initialX;
            directionYOne = initialY - endY;
            flipClickFlag();
            angleExists = true;
        }
        if (!clickFlag) {
            clickFlag = true;
            return drawLine(stack, color, lineWidth, imageViewer);
        }
        return stack;
    }
    private void setLengthOfLine() {
        lengthOfNewLine = Math.sqrt(Math.pow(getBiggerValue(initialX, endX) - getSmalerValue(initialX, endX), 2) +
                Math.pow(getBiggerValue(initialY, endY) - getSmalerValue(initialY, endY), 2));
    }
    private void setSumOfLineLength() {
        lenghtOfAllLines = lenghtOfAllLines + lengthOfNewLine;
    }
    Boolean getClickFlagValue() {
        return clickFlag;
    }

    private StackPane drawLine(StackPane stack, Paint color, int lineWidth, ImageView imageViewer) {
        Canvas layer = new Canvas();
        layer.setHeight(stack.getHeight());
        layer.setWidth(stack.getWidth());
        GraphicsContext line = layer.getGraphicsContext2D();
        line.setStroke(color);
        line.setLineWidth(lineWidth);
        line.strokeLine(initialX, initialY, endX, endY);
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        Image snapshot = layer.snapshot(parameters, null);
        ImageView snapshotViewer = new ImageView();
        snapshotViewer.fitHeightProperty().bind(imageViewer.fitHeightProperty());
        snapshotViewer.fitWidthProperty().bind(imageViewer.fitWidthProperty());
        snapshotViewer.setPreserveRatio(true);
        snapshotViewer.setImage(snapshot);
        stack.getChildren().add(snapshotViewer);
        return stack;
    }

    private StackPane drawArc(StackPane stack, Paint color, int lineWidth, ImageView imageViewer) {
        Canvas layer = new Canvas();
        layer.widthProperty().bind(stack.widthProperty());
        layer.heightProperty().bind(stack.heightProperty());
        GraphicsContext arc = layer.getGraphicsContext2D();
        arc.setStroke(color);
        arc.setLineWidth(lineWidth);
        arc.strokeArc(initialX - arcSize / 2, initialY - arcSize / 2, arcSize, arcSize, startAngle, extendAngle, ArcType.OPEN);
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        Image snapshot = layer.snapshot(parameters, null);
        ImageView snapshotViewer = new ImageView();
        snapshotViewer.fitHeightProperty().bind(imageViewer.fitHeightProperty());
        snapshotViewer.fitWidthProperty().bind(imageViewer.fitWidthProperty());
        snapshotViewer.setPreserveRatio(true);
        snapshotViewer.setImage(snapshot);
        stack.getChildren().add(snapshotViewer);
        return stack;
    }
    public void flipClickFlag() {
        clickFlag = (clickFlag == false) ? true : false;
    }
    private void computeAngles() {
        //firstSector
        if ((directionXOne >= 0 && directionYOne > 0) &&
                (directionXTwo >= 0 && directionYTwo > 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne));
            extendAngle = Math.toDegrees(Math.atan(directionYTwo / directionXTwo)) - startAngle;
        }
        //secondSektor
        else if ((directionXOne < 0 && directionYOne > 0) &&
                (directionXTwo < 0 && directionYTwo > 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne)) + 180;
            extendAngle = Math.toDegrees(Math.atan(directionYTwo / directionXTwo)) - startAngle + 180;
        }
        //thirdSector
        else if ((directionXOne < 0 && directionYOne < 0) &&
                (directionXTwo < 0 && directionYTwo < 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne)) + 180;
            extendAngle = Math.toDegrees(Math.atan(directionYTwo / directionXTwo)) - startAngle + 180;
        }
        //fourthSector
        else if ((directionXOne > 0 && directionYOne < 0) &&
                (directionXTwo > 0 && directionYTwo < 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne));
            extendAngle = Math.toDegrees(Math.atan(directionYTwo / directionXTwo)) - startAngle;
        }
        //firstAndSecondSector
        else if ((directionXOne >= 0 && directionYOne > 0) &&
                (directionXTwo < 0 && directionYTwo > 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 180 + (Math.toDegrees(Math.atan(directionYTwo / directionXTwo)));
            extendAngle = endAngle - startAngle;
        }
        //secondAndFirstSector
        else if ((directionXOne < 0 && directionYOne > 0) &&
                (directionXTwo > 0 && directionYTwo > 0)) {
            startAngle = 180 + Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = (Math.toDegrees(Math.atan(directionYTwo / directionXTwo)));
            extendAngle = endAngle - startAngle;
        }
        //secondAndThirdSector or thirdAndSecondSector
        else if (((directionXOne < 0 && directionYOne > 0) &&
                (directionXTwo < 0 && directionYTwo < 0)) ||
                (directionXOne < 0 && directionYOne < 0) &&
                        (directionXTwo < 0 && directionYTwo > 0)) {
            startAngle = 180 + Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 180 + (Math.toDegrees(Math.atan(directionYTwo / directionXTwo)));
            extendAngle = endAngle - startAngle;
        }
        //thirdAndFourthSector
        else if ((directionXOne < 0 && directionYOne < 0) &&
                (directionXTwo > 0 && directionYTwo < 0)) {
            startAngle = 180 + Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 360 + (Math.toDegrees(Math.atan(directionYTwo / directionXTwo)));
            extendAngle = endAngle - startAngle;
        }
        //fourthAndThirdSector
        else if ((directionXOne > 0 && directionYOne < 0) &&
                (directionXTwo < 0 && directionYTwo < 0)) {
            startAngle = 360 + Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 180 + (Math.toDegrees(Math.atan(directionYTwo / directionXTwo)));
            extendAngle = endAngle - startAngle;
        }
        //firstAndFourthSector or fourthAndFirstSector
        else if (((directionXOne >= 0 && directionYOne > 0) &&
                (directionXTwo > 0 && directionYTwo < 0)) ||
                (directionXOne > 0 && directionYOne < 0) &&
                        (directionXTwo >= 0 && directionYTwo > 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = (Math.toDegrees(Math.atan(directionYTwo / directionXTwo)));
            extendAngle = endAngle - startAngle;
        }
        //fourthAndFirstSector
        else if ((directionXOne > 0 && directionYOne < 0) &&
                (directionXTwo >= 0 && directionYTwo > 0)) {
            startAngle = 360 + Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = Math.toDegrees(Math.atan(directionYTwo / directionXTwo));
            extendAngle = 360 + endAngle - startAngle;
        }
        //firstAndThirdSector
        else if ((directionXOne > 0 && directionYOne > 0) &&
                (directionXTwo < 0 && directionYTwo < 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 180 + Math.toDegrees(Math.atan(directionYTwo / directionXTwo));
            if (endAngle - startAngle < 180) {
                extendAngle = endAngle - startAngle;
            } else extendAngle = -(360 + startAngle - endAngle);
        }
        //thirdAndFirstSector
        else if ((directionXOne < 0 && directionYOne < 0) &&
                (directionXTwo > 0 && directionYTwo > 0)) {
            startAngle = (180 + Math.toDegrees(Math.atan(directionYOne / directionXOne)));
            endAngle = Math.toDegrees(Math.atan(directionYTwo / directionXTwo));
            if (endAngle - startAngle > -180) {
                extendAngle = endAngle - startAngle;
            } else extendAngle = 360 - startAngle + endAngle;
        }
        //secondAndFourthSector
        else if ((directionXOne < 0 && directionYOne > 0) &&
                (directionXTwo > 0 && directionYTwo < 0)) {
            startAngle = 180 + Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 360 + Math.toDegrees(Math.atan(directionYTwo / directionXTwo));
            if (endAngle - startAngle < 180) {
                extendAngle = endAngle - startAngle;
            } else extendAngle = -(360 - endAngle + startAngle);
        }
        //fourthAndSecondSector
        else if ((directionXOne > 0 && directionYOne < 0) &&
                (directionXTwo < 0 && directionYTwo > 0)) {
            startAngle = Math.toDegrees(Math.atan(directionYOne / directionXOne));
            endAngle = 180 + Math.toDegrees(Math.atan(directionYTwo / directionXTwo));
            if (endAngle - startAngle < 180) {
                extendAngle = endAngle - startAngle;
            } else extendAngle = -(360 - endAngle + startAngle);
        }
    }


    private double getBiggerValue(double a, double b) {
        return (a > b) ? a : b;
    }
    private double getSmalerValue(double a, double b) {
        return (a > b) ? b : a;
    }
}