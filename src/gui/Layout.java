package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Layout extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage window) {

        Components components = new Components(window);
        Logic logic = new Logic(components);

        logic.start(window);


        // Layout the components
        GridPane g = new GridPane();
        g.add(components.getCenter(), 1, 0, 3, 3);
        g.add(components.getMenuContainer(), 0, 0, 1, 1);

        Scene scene = new Scene(g, 800, 470);
        window.setScene(scene);

        System.out.println(components.getMenuContainer().getHeight());

        window.setTitle("Title of the program");
        window.show();


        components.getImageViewer().fitHeightProperty().bind(scene.heightProperty());
        components.getImageViewer().fitWidthProperty().bind(scene.widthProperty().subtract(components.getMenuContainer().getWidth()));

        components.getMenuContainer().prefHeightProperty().bind(scene.heightProperty());

    }
}

