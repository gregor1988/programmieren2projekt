package gui;

import editing.TextScannerFromTextFile;
import editing.TextScannerFromXML;
import editing.TextScannerInterface;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class Logic extends Application {
    protected Components components;
    private Image image;
    private Draw draw;
    private TextScannerInterface metaDataFile;
    private double imageMaxHeight;
    private double imageMaxWidth;

    public Logic(Components components) {
        this.components = components;
        this.draw = new Draw(components);
    }

    static void main(String[] args) {
        launch(args);
    }

    private void setLineLengths() {
        try {
            components.editData(2, String.format("%.2f", draw.lengthOfNewLine * metaDataFile.getResolution()) + metaDataFile.getDimension());
            components.editData(3, String.format("%.2f", draw.lenghtOfAllLines * metaDataFile.getResolution()) + metaDataFile.getDimension());
        } catch (Exception e2) {
            components.editData(2, String.format("%.2f", draw.lengthOfNewLine) + "Pixel");
            components.editData(3, String.format("%.2f", +draw.lenghtOfAllLines) + "Pixel");
        }
    }

    public AnchorPane getCenter() {
        return components.getCenter();
    }
    public VBox getMenuContainer() {
        return components.getMenuContainer();
    }
    public HBox getMeasureContainer() {
        return components.getBottomBox();
    }
    public StackPane getStack() {
        return components.getStack();
    }
    HBox getMetaDataContainer() {
        return components.getMetaDataContainer();
    }

    public TextScannerInterface getMetaDataFile() {
        return metaDataFile;
    }
    @Override
    public void start(Stage window) {
        components.getLoadButton().setOnAction(event -> {
            components.clearStack();
            FileChooser fileChooser = new FileChooser();
            File selectedFolder = fileChooser.showOpenDialog(window);
            try {
                String metaDataFilePath = selectedFolder.getPath();
                String parentPathOfMetaDataFile = selectedFolder.getParent();
                StringBuilder imagePath = new StringBuilder(parentPathOfMetaDataFile);
                imagePath.append("\\");
                if (metaDataFilePath.toString().endsWith(".txt")) {
                    metaDataFile = new TextScannerFromTextFile(metaDataFilePath);
                    imagePath.append(metaDataFile.getImageName());
                    image = metaDataFile.setImage(imagePath.toString());
                } else if (metaDataFilePath.toString().endsWith(".xml")) {
                    metaDataFile = new TextScannerFromXML(metaDataFilePath);
                    imagePath.append(metaDataFile.getImageName());
                    image = metaDataFile.setImage(imagePath.toString());
                } else {
                    components.getWrongFileEndingAlert().show();
                }
                this.imageMaxHeight = metaDataFile.getHeight();
                this.imageMaxWidth = metaDataFile.getHeight();
                components.getImageViewer().setImage(image);
                components.getDescriptionTextLabel().setText(metaDataFile.getDescription());
                components.getNameOfImageTextLabel().setText(metaDataFile.getImageName());
                components.editData(2, "-");
                components.editData(3, "-");
                components.editData(4, "-");

                try {
                    components.editData(0, String.format("%.2f", metaDataFile.getHeight() * metaDataFile.getResolution()) + metaDataFile.getDimension());
                    components.editData(1, String.format("%.2f", metaDataFile.getWidth() * metaDataFile.getResolution()) + metaDataFile.getDimension());
                } catch (Exception e) {
                    components.editData(0, String.format("%.2f", metaDataFile.getHeight()) + "Pixel");
                    components.editData(1, String.format("%.2f", metaDataFile.getWidth()) + "Pixel");
                }
            } catch (Exception e) {
            }
        });
        components.getColorPicker().setOnAction(event ->
                components.setColor(components.getColorPicker().getValue()));
        components.getLineWidthChooser().valueProperty().addListener(observable ->
                components.setLineWidth((int) components.getLineWidthChooser().getValue()));
        components.getSingleLinesButton().setOnAction(event -> {
            if (draw.getClickFlagValue()) {
                draw.flipClickFlag();
            }
        });
        components.getContinuousLineButton().setOnAction(event -> {
            if (draw.getClickFlagValue()) {
                draw.flipClickFlag();
            }
        });
        components.getMeasureAngleButton().setOnAction(event -> {
            if (draw.getClickFlagValue()) {
                draw.flipClickFlag();
            }
        });
        components.getStack().setOnMousePressed(e -> {
            if (components.getRadioButtonGroup().getSelectedToggle() == components.getSingleLinesButton()) {
                draw.drawSingleLineMode(e, components.getStack(), components.getColor(), components.getLineWidth(), components.getImageViewer());
                if (!draw.getClickFlagValue()) {
                    setLineLengths();
                }
            } else if (components.getRadioButtonGroup().getSelectedToggle() == components.getContinuousLineButton()) {
                draw.drawContinuousLineMode(e, components.getStack(), components.getColor(), components.getLineWidth(), components.getImageViewer());
                setLineLengths();
            } else if (components.getRadioButtonGroup().getSelectedToggle() == components.getMeasureAngleButton()) {
                draw.measureAngleMode(e, components.getStack(), components.getColor(), components.getLineWidth(), components.getImageViewer());
                components.editData(4, String.format("%.2f", draw.extendAngle) + "°");
            }
        });
    }
}
