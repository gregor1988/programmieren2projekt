package gui;/**
 * Created by Greg on 17.05.2017.
 */

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.awt.*;
import java.util.Arrays;

public class Components extends Application {
    //Buttons
    private Button loadButton = new Button("Load image...");
    private RadioButton singleLinesButton = new RadioButton("draw single lines");
    private RadioButton continuousLineButton = new RadioButton("draw continuous line");
    private RadioButton measureAngleButton = new RadioButton("measure angles");
    //Groups
    private ToggleGroup radioButtonGroup = new ToggleGroup();
    //Labels
    private Label nameOfImageTextLabel = new Label();
    private Label descriptionTextLabel = new Label();
    //Boxes
    private HBox metaDataContainer = new HBox();
    private HBox bottomBox = new HBox();
    private VBox rightBox = new VBox();
    private VBox radioMenuBox = new VBox();
    private VBox menuContainer = new VBox();
    private VBox tableContainer = new VBox();
    //ImageView
    private ImageView imageViewer = new ImageView();
    //Panes
    private StackPane stack = new StackPane(imageViewer);
    private AnchorPane center = new AnchorPane(stack);
    //other Components
    private ColorPicker colorPicker = new ColorPicker(Color.BLACK);
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private Paint color;
    private ScrollBar lineWidthChooser = new ScrollBar();
    private int lineWidth;
    private Alert wrongFileEndingAlert = new Alert(Alert.AlertType.ERROR, "Can only load from *.txt or *.xml files.");
    //tableView + cells
    private TableView<String[]> table = new TableView();
    private TableColumn<String[], String> identifierColumn = new TableColumn();
    private TableColumn<String[], String> valueColumn = new TableColumn();
    private String[][] data = {
            {"heigth:", "-"},
            {"width:", "-"},
            {"length of last line:", "-"},
            {"length of all lines:", "-"},
            {"angle:", "-"}
    };


    public Components(Stage window) {

        this.radioMenuBox.getChildren().addAll(singleLinesButton,
                continuousLineButton,
                measureAngleButton);
        this.radioMenuBox.setSpacing(5);


        this.menuContainer.setSpacing(20);
        this.menuContainer.setPadding(new Insets(50, 10, 10, 10));
        this.menuContainer.setStyle("-fx-background-color: #9CADB5;");
        this.menuContainer.getChildren().addAll(tableContainer, loadButton, radioMenuBox, colorPicker, lineWidthChooser);
        this.menuContainer.setAlignment(Pos.CENTER);

        this.nameOfImageTextLabel.setMinWidth(130);
        this.nameOfImageTextLabel.setPadding(new Insets(5, 5, 5, 5));
        this.nameOfImageTextLabel.setText("Name of the image: ");
        this.nameOfImageTextLabel.setTextAlignment(TextAlignment.CENTER);
        this.nameOfImageTextLabel.setStyle("-fx-background-color: #E5E3DF;");

        this.descriptionTextLabel.setMinWidth(250);
        this.descriptionTextLabel.setPadding(new Insets(5, 5, 5, 5));
        this.descriptionTextLabel.setText("Description: ");
        this.descriptionTextLabel.setTextAlignment(TextAlignment.CENTER);
        this.descriptionTextLabel.setStyle("-fx-background-color: #E5E3DF;");


        this.table.setEditable(true);
        this.identifierColumn.setCellValueFactory((p) -> {
            String[] x = p.getValue();
            return new SimpleStringProperty(x != null && x.length > 0 ? x[0] : "");
        });

        this.valueColumn.setCellValueFactory((p) -> {
            String[] x = p.getValue();
            return new SimpleStringProperty(x != null && x.length > 1 ? x[1] : "");
        });
        this.identifierColumn.setSortable(false);
        this.valueColumn.setSortable(false);
        this.valueColumn.setPrefWidth(90);
        this.table.getColumns().addAll(identifierColumn, valueColumn);
        this.table.getItems().addAll(Arrays.asList(data));
        this.table.setMaxHeight(30 * 5 + 27);
        this.table.setMinHeight(30 * 5 + 27);

        this.tableContainer.getChildren().add(table);
        this.tableContainer.setStyle("-fx-background-color: #9cadb5");

        this.rightBox.setStyle("-fx-background-color: #9cadb5");

        this.bottomBox.setStyle("-fx-background-color: #9cadb5");

        this.metaDataContainer.setStyle("-fx-background-color: #9cadb5");
        this.metaDataContainer.setPadding(new Insets(10, 10, 10, menuContainer.getMinWidth()));
        this.metaDataContainer.setSpacing(50);
        this.metaDataContainer.setAlignment(Pos.CENTER_LEFT);
        this.metaDataContainer.getChildren().addAll(getDescriptionTextLabel(), getNameOfImageTextLabel());

        this.lineWidthChooser.setUnitIncrement(1);
        this.lineWidthChooser.setMin(1);
        this.lineWidthChooser.setMax(10);

        this.imageViewer.setPreserveRatio(true);

        this.stack.setMinSize(0, 0);
        this.stack.setAlignment(Pos.TOP_LEFT);
        this.stack.setMinHeight(0);
        this.stack.setMinWidth(0);
        this.stack.setStyle("-fx-background-color: #9cadb5");
        this.stack.resize(500, 350);
    }

    public void editData(int row, String value) {
        this.data[row][1] = value;
        table.getItems().clear();
        table.getItems().addAll(Arrays.asList(data));
    }

    public Button getLoadButton() {
        return loadButton;
    }

    public RadioButton getSingleLinesButton() {

        return singleLinesButton;
    }

    public RadioButton getContinuousLineButton() {
        return continuousLineButton;
    }

    public RadioButton getMeasureAngleButton() {
        return measureAngleButton;
    }

    public Label getDescriptionTextLabel() {
        return descriptionTextLabel;
    }

    public Label getNameOfImageTextLabel() {

        return nameOfImageTextLabel;
    }

    public ImageView getImageViewer() {
        return imageViewer;
    }

    public ToggleGroup getRadioButtonGroup() {
        singleLinesButton.setToggleGroup(radioButtonGroup);
        continuousLineButton.setToggleGroup(radioButtonGroup);
        measureAngleButton.setToggleGroup(radioButtonGroup);
        return radioButtonGroup;
    }

    public HBox getMetaDataContainer() {
        return metaDataContainer;
    }

    public HBox getBottomBox() {
        return bottomBox;
    }

    public VBox getRightBox() {
        return rightBox;
    }

    public VBox getMenuContainer() {
        return menuContainer;
    }

    public StackPane getStack() {
        return stack;
    }

    public AnchorPane getCenter() {
        return center;
    }

    public ColorPicker getColorPicker() {
        return colorPicker;
    }

    public Paint getColor() {
        return color;
    }

    public void setColor(Paint color) {
        this.color = color;
    }

    public ScrollBar getLineWidthChooser() {
        return lineWidthChooser;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Alert getWrongFileEndingAlert() {
        return wrongFileEndingAlert;
    }

    public void clearStack() {
        stack.getChildren().clear();
        stack.getChildren().add(imageViewer);
    }

    @Override
    public void start(Stage primaryStage) {


    }
}
